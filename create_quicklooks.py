#!/usr/bin/env python3
import os
import logging
import json
import sys
import tarfile

logging.basicConfig(
        stream=sys.stdout,
        level=logging.DEBUG,  # TODO set it from container level
        format="%(asctime)s [%(levelname)s] [%(name)s::%(lineno)d] %(message)s",
    )
logger = logging.getLogger(__name__)


def get_download_info():
    ctx = json.loads(open("_context.json").read())
    localize_url = ctx.get("localize_urls")[0]
    if type(localize_url) is dict:
        url = localize_url.get("url")
    elif type(localize_url) is str:
        url = localize_url
    return url, ctx.get("band_num")


def unzip_file(file_path):
    logger.info("Opening tar file {}".format(file_path))
    file = tarfile.open(file_path)
    main_file = os.path.splitext(os.path.basename(file_path))[0].split(".")[0]
    logger.info("main file {}".format(main_file))
    # extracting a specific file
    logger.info("Extracting main file to work directory")
    file.extractall('.')
    file.close()
    return main_file


def main():
    """
    1. Read context for localize_urls
    2. Find base of URL
    :return:
    """
    url, band = get_download_info()
    logger.info("Download product from {}".format(url))
    download_file_name = os.path.basename(url)
    if download_file_name in os.listdir("input"):
        logger.info("Found {} in input directory".format(download_file_name))
        if download_file_name.endswith(".tar.gz"):
            # unzip the tar
            main_file = unzip_file("input/{}".format(download_file_name))
        else:
            for file in os.listdir("input/{}".format(download_file_name)):
                if file.endswith(".tar.gz"):
                    main_file = unzip_file("input/{}/{}".format(download_file_name, file))
        # run gdal command
        os.mkdir("output")
        logger.info("Executing gdal command:")
        logger.info(("gdal_translate -scale {}/{} output/{}.png -co ZLEVEL=9 -of PNG -ot BYTE -exponent 0.5 -b {}"
                     .format(main_file, main_file, main_file, band)))
        os.system("gdal_translate -scale {}/{} output/{}.png -co ZLEVEL=9 -of PNG -ot BYTE -exponent 0.5 -b {}"
                  .format(main_file, main_file,  main_file, band))


if __name__ == '__main__':
    main()
